/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi.oad;

import java.util.List;
import mx.lania.rysi16.cattle4f2s.entidades.cliente;

/**
 *
 * @author gerardosuarez
 */
public interface OadClientes {

    void crear (cliente cliente);
    void actualizar (cliente cliente);
    List<cliente> buscarPorNombre(String nombre);

    public List<cliente> findAll();
    
}
