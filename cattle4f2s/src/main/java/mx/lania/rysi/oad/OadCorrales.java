/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi.oad;

import java.util.List;
import mx.lania.rysi16.cattle4f2s.entidades.corrales;

/**
 *
 * @author gerardosuarez
 */
public interface OadCorrales {

    void crear (corrales corral);
    void actualizar (corrales corral);
    List<corrales> buscarPorNombre(String nombre);
    
}
