/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi.oad;

import java.util.List;
import mx.lania.rysi16.cattle4f2s.entidades.ordendecompra;

/**
 *
 * @author gerardosuarez
 */
public interface OadOrdenDeCompra {

    void crear(ordendecompra orden);
    void actualizar(ordendecompra orden);
    List<ordendecompra> buscarPorNombre(String nombre);
    
}
