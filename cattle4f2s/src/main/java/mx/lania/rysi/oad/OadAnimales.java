/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi.oad;

import java.util.List;
import mx.lania.rysi16.cattle4f2s.entidades.Animal;

/**
 *
 * @author gerardosuarez
 */
public interface OadAnimales {
    
    void crear (Animal Animales);
    void actualizar (Animal Animales);
    List<Animal> findByAreteStartingWithIgnoreCase(String cadena);
    

    public List<Animal> findAll();
            
    
}
