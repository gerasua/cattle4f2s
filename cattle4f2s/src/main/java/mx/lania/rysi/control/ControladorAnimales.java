/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi.control;

import java.util.List;
import mx.lania.rysi.oad.OadAnimales;
import mx.lania.rysi16.cattle4f2s.entidades.Animal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author gerardosuarez
 */
@RestController
@RequestMapping("/animales")
public class ControladorAnimales {
    private static Logger logger = LoggerFactory.getLogger(ControladorAnimales.class);
    
    @Autowired
    OadAnimales oadAnimales;
    
    @RequestMapping("")
    public List<Animal> getAnimales() {
        logger.debug("GET /animales");
        return oadAnimales.findAll();
    }
    
}
