/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi.control;

import java.util.List;
import mx.lania.rysi.oad.OadClientes;
import mx.lania.rysi16.cattle4f2s.entidades.cliente;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author gerardosuarez
 */
@RestController
@RequestMapping("/clientes")
public class ControladorClientes {
    private static Logger logger = LoggerFactory.getLogger(ControladorClientes.class);
    
    @Autowired
    OadClientes oadClientes;
    
    @RequestMapping("")
    public List<cliente> getClientes() {
        logger.debug("GET /clientes");
        return oadClientes.findAll();
    }
    
}
