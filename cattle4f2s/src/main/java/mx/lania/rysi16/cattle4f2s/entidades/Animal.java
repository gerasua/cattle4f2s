/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi16.cattle4f2s.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author gerardosuarez
 */
@Entity
@Table(name = "ANIMALES")
public class Animal implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ANIMAL")
    private Integer idAnimal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10000)
    @Column(name = "ARETE")
    private String arete;
    @Size(max = 1000)
    @Column(name = "CORRAL")
    private Integer corral;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "PESO")
    private BigDecimal peso;

    public Animal() {
        
    }
    public Animal(Integer idAnimal) {
        this.idAnimal = idAnimal;
    }

    public Animal(Integer idAnimal, String arete, Integer corral, BigDecimal peso) {
        this.idAnimal = idAnimal;
        this.arete = arete;
        this.corral = corral;
        this.peso = peso;
    }

    public Integer getIdAnimal() {
        return idAnimal;
    }
     public void setIdAnimal(Integer idAnimal) {
        this.idAnimal = idAnimal;
    }
    
    public String getArete() {
        return arete;
    }

    public void setArete(String nombre) {
        this.arete = nombre;
    }
    
    public Integer getCorral(){
        return corral;
    }
    
    public void setCorral(Integer corral){
        this.corral = corral;
    }
    
    public BigDecimal getPeso() {
        return peso;
    }

    public void setPeso(BigDecimal peso) {
        this.peso = peso;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAnimal != null ? idAnimal.hashCode() : 0);
        return hash;
    }
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Animal)) {
            return false;
        }
        Animal other = (Animal) object;
        if ((this.idAnimal == null && other.idAnimal != null) || (this.idAnimal != null && !this.idAnimal.equals(other.idAnimal))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "mx.lania.rysi16.cattle4f2s.entidades.Animal[ idAnimal=" + idAnimal + " ]";
    }
    
}
